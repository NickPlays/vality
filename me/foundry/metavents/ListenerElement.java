package me.foundry.metavents;

import me.foundry.metavents.filter.Filter;

import java.lang.annotation.Annotation;
import java.util.function.Consumer;

/**
 * @author Foundry
 */
public interface ListenerElement<T> {
    Consumer<T> getInvoker();

    Priority getPriority();

    Filter<?>[] getFilters();

    Class<T> getTarget();

    <A extends Annotation> A getAnnotation(Class<A> annotationClass);

    String getName();
}
