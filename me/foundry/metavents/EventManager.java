package me.foundry.metavents;

import me.foundry.metavents.filter.Filter;
import me.foundry.metavents.filter.Filtered;
import me.foundry.metavents.internal.factory.LambdaFactory;
import me.foundry.metavents.internal.factory.exceptions.FactoryException;
import me.foundry.metavents.internal.util.ConcurrentWeakHashMap;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Set;
import java.util.SortedSet;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.ConcurrentSkipListSet;
import java.util.function.Consumer;
import java.util.stream.Collectors;

/**
 * @author Foundry
 */
public class EventManager<T> {
    private final ConcurrentMap<Class<T>, SortedSet<ListenerElement<T>>> eventToListenerMap = new ConcurrentHashMap<>();

    private final ConcurrentMap<Object, Set<ListenerElement<T>>> instanceListenerCache = new ConcurrentWeakHashMap<>();

    private final Class<?> eventTarget;

    public EventManager(Class<T> eventTarget) {
        this.eventTarget = eventTarget;
    }

    public EventManager() {
        this.eventTarget = Object.class;
    }

    @SafeVarargs
    public final boolean subscribe(Object parent, Class<? extends T>... events) {
        boolean added = false;
        for (ListenerElement<T> invokable : instanceListenerCache.computeIfAbsent(parent, p -> Arrays.stream(p.getClass().getDeclaredMethods())
                .filter(method -> method.isAnnotationPresent(Listener.class))
                .filter(method -> method.getParameterCount() == 1 && eventTarget.isAssignableFrom(method.getParameterTypes()[0]))
                .map(method -> generateDynamicInvoker(p, method))
                .collect(Collectors.toSet()))) {
            if (events.length > 0) {
                final SortedSet<ListenerElement<T>> registeredInvokables = eventToListenerMap.computeIfAbsent(invokable.getTarget(), s -> new ConcurrentSkipListSet<>());
                for (Class<? extends T> event : events) {
                    if (invokable.getTarget() == event) {
                        registeredInvokables.add(invokable);
                        added = true;
                        break;
                    }
                }
            } else {
                eventToListenerMap.computeIfAbsent(invokable.getTarget(), s -> new ConcurrentSkipListSet<>()).add(invokable);
                added = true;
            }
        }
        return added;
    }

    @SafeVarargs
    public final boolean unsubscribe(Object parent, Class<T>... events) {
        boolean removed = false;
        final Set<ListenerElement<T>> methodCacheLookup = instanceListenerCache.get(parent.getClass());
        if (methodCacheLookup != null) {
            for (ListenerElement<T> invokable : methodCacheLookup) {
                final SortedSet<ListenerElement<T>> registeredInvokables = eventToListenerMap.get(invokable.getTarget());
                if (registeredInvokables != null && registeredInvokables.size() > 0) {
                    if (events.length > 0) {
                        for (Class<T> event : events) {
                            if (invokable.getTarget() == event) {
                                registeredInvokables.remove(invokable);
                                removed = true;
                                break;
                            }
                        }
                    } else {
                        registeredInvokables.remove(invokable);
                        removed = true;
                    }
                }
            }
        }
        return removed;
    }

    @SuppressWarnings("unchecked")
    public T post(T event) {
        final SortedSet<ListenerElement<T>> invokableLookup = eventToListenerMap.get(event.getClass());
        if (invokableLookup != null && invokableLookup.size() > 0) {
            OUTER:
            for (ListenerElement<T> invokable : invokableLookup) {
                if (invokable.getFilters().length > 0) {
                    for (Filter filter : invokable.getFilters()) {
                        if (!filter.test(event, invokable)) continue OUTER;
                    }
                }
                invokable.getInvoker().accept(event);
            }
        }
        return event;
    }

    @SuppressWarnings("unchecked")
    private ListenerElement<T> generateDynamicInvoker(Object parent, Method method) {
        try {
            return new BaseListenerElement<>(LambdaFactory.create(Consumer.class, parent.getClass(), parent, method),
                    (Class<T>) method.getParameterTypes()[0],
                    method);
        } catch (FactoryException e) {
            throw new RuntimeException("Exception creating dynamic invoker for method \"" + method.getName() + "\":\n" + e);
        }
    }

    private static class BaseListenerElement<T> implements ListenerElement<T>, Comparable<BaseListenerElement<T>> {
        private final Consumer<T> invoker;
        private final Class<T> eventTarget;
        private final Priority priority;
        private final Filter[] filters;
        private final Annotation[] annotations;
        private final String name;

        BaseListenerElement(Consumer<T> invoker, Class<T> eventTarget, Method originalMethod) {
            this.invoker = invoker;
            this.eventTarget = eventTarget;
            this.priority = originalMethod.getDeclaredAnnotation(Listener.class).value();
            this.annotations = originalMethod.getAnnotations();
            this.name = originalMethod.getName();

            Filtered filteringAnnotation = originalMethod.getDeclaredAnnotation(Filtered.class);
            if (filteringAnnotation != null) {
                Filter[] workingFilters = new Filter[filteringAnnotation.value().length];
                try {
                    for (int i = 0; i < filteringAnnotation.value().length; i++) {
                        validateFilter(filteringAnnotation.value()[i], eventTarget, originalMethod);
                        workingFilters[i] = filteringAnnotation.value()[i].newInstance();
                    }
                } catch (InstantiationException | IllegalAccessException e) {
                    this.filters = new Filter[0];
                    throw new RuntimeException("Access exception encountered creating filters for method \"" + originalMethod.getName() + "\": " + e);
                }
                this.filters = workingFilters;
            } else {
                this.filters = new Filter[0];
            }
        }

        @Override
        public Consumer<T> getInvoker() {
            return this.invoker;
        }

        @Override
        public Priority getPriority() {
            return this.priority;
        }

        @Override
        public Filter[] getFilters() {
            return this.filters;
        }

        @Override
        public Class<T> getTarget() {
            return this.eventTarget;
        }

        @Override
        public String getName() {
            return this.name;
        }

        @Override
        @SuppressWarnings("unchecked")
        public <A extends Annotation> A getAnnotation(Class<A> annotationClass) {
            for (Annotation annotation : this.annotations) {
                if (annotation.annotationType() == annotationClass) {
                    return (A) annotation;
                }
            }
            return null;
        }

        @Override
        public int compareTo(BaseListenerElement<T> o) {
            final int discriminant = Integer.compare(this.priority.ordinal(), o.priority.ordinal());
            return discriminant == 0 ? System.identityHashCode(o) - System.identityHashCode(this) : discriminant;
        }

        private static void validateFilter(Class<? extends Filter> filterClass, Class<?> expectedTarget, Method parentMethod) {
            for (Type type : filterClass.getGenericInterfaces()) {
                if (type instanceof ParameterizedType && ((ParameterizedType) type).getRawType() == Filter.class) {
                    Class<?> filterTarget = (Class<?>) ((ParameterizedType) type).getActualTypeArguments()[0];
                    if (!filterTarget.isAssignableFrom(expectedTarget)) {
                        throw new IllegalArgumentException("Exception creating filters for method \"" + parentMethod.getName()
                                + "\": Filter " + filterClass.getSimpleName()
                                + " targets type " + filterTarget.getSimpleName()
                                + ", but listener event target is of type " + expectedTarget.getSimpleName());
                    }
                }
            }
        }
    }
}
