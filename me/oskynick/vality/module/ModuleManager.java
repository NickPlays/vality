package me.oskynick.vality.module;

import me.oskynick.vality.module.none.Click;
import me.oskynick.vality.module.test.TestStep;

import java.util.ArrayList;

/**
 * Created by Nick on 2/11/2017.
 */
public class ModuleManager {

    public ArrayList<Module> mods = new ArrayList<Module>();

    public ModuleManager(){
        mods.add(new TestStep());
        mods.add(new Click());
    }
    
    public ArrayList<Module> getMods(Category cat) {
		ArrayList<Module> modsInCat = new ArrayList<Module>();
		for (Module m : mods) {
			if (m.getCategory() == cat) {
				modsInCat.add(m);
			}
		}
		return modsInCat;
	}
}
