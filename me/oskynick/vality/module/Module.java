package me.oskynick.vality.module;

import me.oskynick.vality.Vality;
import org.lwjgl.input.Keyboard;

import java.lang.annotation.*;

/**
 * Created by Nick on 2/11/2017.
 */
public class Module {

    public String name;
    public String description;
    public int keybind;
    public static Category category;
    public boolean active;

    @Target(ElementType.TYPE)
    @Retention(RetentionPolicy.RUNTIME)
    public @interface Info {
        String name() default "Unspecified";
        Category category() default Category.NONE;
        String description() default "Unspecified";
        int keybind() default Keyboard.KEY_NONE;
    }

    public Module(){
        name = getClass().getAnnotation(Info.class).name();
        category = getClass().getAnnotation(Info.class).category();
        description = getClass().getAnnotation(Info.class).description();
        keybind = getClass().getAnnotation(Info.class).keybind();
    }

    public String getName(){
        return name;
    }

    public Category getCategory(){
        return category;
    }

    public String getDescription(){
        return description;
    }

    public int getKeybind(){
        return keybind;
    }

    public void onEnable(){}
    public void onDisable(){}
    public void onToggle(){}

    public void setActive(boolean active){
        this.active = active;
        onToggle();

        if(active){
            Vality.metaventManager.subscribe(this);
            onEnable();
        }else{
            Vality.metaventManager.unsubscribe(this);
            onDisable();
        }
    }

    public void toggle(){
        this.setActive(!active);
    }
}
