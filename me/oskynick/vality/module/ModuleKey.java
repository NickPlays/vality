package me.oskynick.vality.module;

import me.foundry.metavents.Listener;
import me.oskynick.vality.Vality;
import me.oskynick.vality.events.EventKey;

/**
 * Created by Nick on 2/12/2017.
 */
public class ModuleKey {

    @Listener
    public void onPress(EventKey event){
        for(Module mod : Vality.getModuleManager().mods){
            if(mod.getKeybind() == event.key){
                mod.toggle();
            }
        }
    }
}
