package me.oskynick.vality.module.none;

import me.oskynick.vality.Vality;
import me.oskynick.vality.module.Category;
import me.oskynick.vality.module.Module;
import net.minecraft.client.Minecraft;
import org.lwjgl.input.Keyboard;

/**
 * Created by Nick on 2/13/2017.
 */
@Module.Info(name = "Click", category = Category.NONE, description = "A nice interface to use.", keybind = Keyboard.KEY_RSHIFT)
public class Click extends Module{

    @Override
    public void onToggle(){
        Minecraft.getMinecraft().displayGuiScreen(Vality.getClickManager());
    }
}
