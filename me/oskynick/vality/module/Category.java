package me.oskynick.vality.module;

/**
 * Created by Nick on 2/11/2017.
 */
public enum Category {

    PLAYER("Player", 0xFFFF00FF),
    WORLD("World", 0xFFe9b822),
    COMBAT("Combat", 0xFFce0429),
    MOVEMENT("Movement", 0xFFeb6d21),
    RENDER("Render", 0xFFb610e6),
    MISC("Misc", 0xFF2bc3c9),
    NONE("None", 0xFFFFFFFF);

    int color;
    String name;
    
    Category(String name, int color){
    	this.name = name;
        this.color = color;
    }
    
    public String getName(){
    	return name;
    } 

    public int getColor(){
        return color;
    }
}
