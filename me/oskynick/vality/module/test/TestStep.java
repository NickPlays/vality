package me.oskynick.vality.module.test;

import me.oskynick.vality.module.Category;
import me.oskynick.vality.module.Module;
import net.minecraft.client.Minecraft;
import org.lwjgl.input.Keyboard;

/**
 * Created by Nick on 2/11/2017.
 */
@Module.Info(name = "TestStep", category = Category.MOVEMENT, description = "A test module", keybind = Keyboard.KEY_Z)
public class TestStep extends Module {

    @Override
    public void onEnable(){
        Minecraft.getMinecraft().player.stepHeight = 1f;
        System.out.println("Enabled");
    }

    @Override
    public void onDisable(){
        Minecraft.getMinecraft().player.stepHeight = .5f;
        System.out.println("Disabled");
    }
}
