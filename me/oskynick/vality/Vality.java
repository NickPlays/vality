package me.oskynick.vality;

import me.foundry.metavents.EventManager;
import me.oskynick.vality.font.CFontManager;
import me.oskynick.vality.module.ModuleKey;
import me.oskynick.vality.module.ModuleManager;
import me.oskynick.vality.ui.UIRenderer;
import me.oskynick.vality.ui.click.ClickManager;
import me.oskynick.vality.utils.AuthUtils;

public class Vality {
	
	public String name = "Vality";
	public double version = 0.1d;
	public String author = "oskynick";
	private static ModuleManager mod;
	private static ClickManager click;
	public static final EventManager<Object> metaventManager = new EventManager<>();
	
	public Vality(){
		try {
			CFontManager.load();
			metaventManager.subscribe(new UIRenderer());
			metaventManager.subscribe(new ModuleKey());
			mod = new ModuleManager();
			click = new ClickManager();
			AuthUtils.Login("xxthetruecoder@gmail.com", "321heatx");
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	public String getName(){
		return name;
	}
	public static ModuleManager getModuleManager(){
		return mod;
	}
	public static ClickManager getClickManager(){
		return click;
	}
	public double getVersion(){
		return version;
	}
	public String getAuthor(){
		return author;
	}

}
