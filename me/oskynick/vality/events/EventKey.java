package me.oskynick.vality.events;

/**
 * Created by Nick on 2/12/2017.
 */
public class EventKey {

    public int key;

    public EventKey(int key){
        this.key = key;
    }

    public int getKey(){
        return key;
    }
}
