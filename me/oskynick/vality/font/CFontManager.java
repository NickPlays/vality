package me.oskynick.vality.font;


import java.awt.Font;
import me.oskynick.vality.font.CFontRenderer;
/**
 * Created by Nick on 2/12/2017.
 */
public class CFontManager {

    public static CFontRenderer font_proximity;
    public static CFontRenderer font_hack;

    public static void load() {
        font_proximity = createFont("/forgotten_futurist_bold.ttf", "Forgotten Futurist Rg", Font.BOLD, 18);
        font_hack = createFont("/Targa MS.ttf", "Hack Bold", Font.BOLD, 18);

    }

    private static CFontRenderer createFont(String path, String fallback, int style, int size) {
        Font f;
        try {
            f = Font.createFont(0, CFontManager.class.getResourceAsStream(path)).deriveFont(style, size);
        } catch (Exception e) {
            f = new Font(fallback, style, size);
        }
        return new CFontRenderer(f, true, false);
    }
}
