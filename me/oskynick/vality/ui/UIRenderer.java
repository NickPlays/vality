package me.oskynick.vality.ui;

import me.foundry.metavents.Listener;
import me.oskynick.vality.Vality;
import me.oskynick.vality.events.EventRender2D;
import me.oskynick.vality.module.Category;
import me.oskynick.vality.module.Module;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.ScaledResolution;

import java.awt.*;

/**
 * Created by Nick on 2/11/2017.
 */
public class UIRenderer {

    private static Minecraft mc = Minecraft.getMinecraft();

    @Listener
    public void renderUI(EventRender2D event) {
        Minecraft.getMinecraft().fontRendererObj.drawStringWithShadow("Vality", 2, 2, 0xFF1E8BC3);
        arrayList();
    }

    public void arrayList(){
        int i = 0;
        ScaledResolution sr = new ScaledResolution(mc);
        for(Module m : Vality.getModuleManager().mods){
            if(m.active && !(m.getCategory() == Category.NONE)){
                int x = (int) (sr.getScaledWidth() - mc.fontRendererObj.getStringWidth(m.getName()) - 2);
                int y = (10 * i) + 2;
                mc.fontRendererObj.drawStringWithShadow(m.getName(), x, y, m.getCategory().getColor());
                i++;
            }
        }
    }
}
