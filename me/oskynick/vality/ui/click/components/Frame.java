package me.oskynick.vality.ui.click.components;

import java.awt.Font;
import java.util.ArrayList;

import me.oskynick.vality.Vality;
import me.oskynick.vality.font.CFontRenderer;
import me.oskynick.vality.ui.click.components.element.Element;
import me.oskynick.vality.utils.RenderUtils;
import net.minecraft.client.Minecraft;
import net.minecraft.client.audio.PositionedSoundRecord;
import net.minecraft.init.SoundEvents;

/**
 * Created by Nick on 2/13/2017.
 */
public class Frame extends Element{

    public ArrayList<Element> elements = new ArrayList<Element>();
    public int dragX, dragY;
    public boolean drag;
    
    public CFontRenderer font = new CFontRenderer(new Font("Arial", Font.BOLD, 18), true, true);

    public Frame(String title, int x, int y){
        this.title = title;
        this.x = x;
        this.y = y;
    }

    public ArrayList<Element> getElements(){
        return this.elements;
    }

    public void drawElement(int mouseX, int mouseY, float partialTicks){
        drag(mouseX, mouseY);
        Vality.getClickManager().getTheme().draw(this, mouseX, mouseY, partialTicks);
    }

    public void mouseClicked(int mouseX, int mouseY, int mouseButton){
    	if(open){
    		for(Element element : getElements()) element.mouseClicked(mouseX, mouseY, mouseButton);
    	}
        
        Vality.getClickManager().getTheme().mouseClicked(this, mouseX, mouseY, mouseButton);
        if(mouseButton == 0 && isInside(mouseX, mouseY)){
            dragX = x - mouseX;
            dragY = y - mouseY;
            drag = true;
            return;
        }
    }

    public void mouseReleased(int mouseX, int mouseY, int state){
    	if(open){
    		for(Element element : getElements()) element.mouseReleased(mouseX, mouseY, state);
    	}

        if(state == 0){
            drag = false;
        }
    }

    public void addElement(Element element){
        elements.add(element);
    }

    private void drag(int mouseX, int mouseY) {
        if (!drag) return;
        x = dragX + mouseX;
        y = dragY + mouseY;
    }
    
    public final boolean isInsideOpenable(int mouseX, int mouseY) {
        return (mouseX >= x + 106 && mouseX <= x + width - 2) && (mouseY >= y + 2 && mouseY <= y + height - 2);
    }
}
