package me.oskynick.vality.ui.click.components.element;

import java.awt.Font;

import me.oskynick.vality.Vality;
import me.oskynick.vality.font.CFontRenderer;
import net.minecraft.client.Minecraft;
import net.minecraft.client.audio.PositionedSoundRecord;
import net.minecraft.init.SoundEvents;

public class Button extends Element{
	
	public CFontRenderer font = new CFontRenderer(new Font("Arial", Font.PLAIN, 18), true, true);
	
	public Button(String title){
		this.title = title;
	}

	@Override
	public void drawElement(int mouseX, int mouseY, float partialTicks){
		this.onUpdate();
		Vality.getClickManager().getTheme().draw(this, mouseX, mouseY, partialTicks);
	}

	@Override
	public void mouseClicked(int mouseX, int mouseY, int mouseButton){
		Vality.getClickManager().getTheme().mouseClicked(this, mouseX, mouseY, mouseButton);
		if (mouseButton == 0 && isInside(mouseX, mouseY)) {
			Minecraft.getMinecraft().getSoundHandler().playSound(PositionedSoundRecord.getMasterRecord(SoundEvents.UI_BUTTON_CLICK, 1.0F));
			this.onToggle();
			this.enabled = !this.enabled;
		}
	}

	@Override
	public void mouseReleased(int mouseX, int mouseY, int state){
	}
	
	public void onUpdate(){}
	public void onToggle(){}

}
