package me.oskynick.vality.ui.click.theme;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;

import me.oskynick.vality.ui.click.components.element.Element;

public abstract class ElementRenderer<T extends Element> {

    private final Class<T> type;

    protected final AbstractTheme theme;

    @SuppressWarnings("unchecked")
    public ElementRenderer(AbstractTheme theme) {
        this.theme = theme;

        Type type = this.getClass().getGenericSuperclass();
        if (type instanceof ParameterizedType) {
            this.type = (Class<T>) ((ParameterizedType) type).getActualTypeArguments()[0];
        } else {
            this.type = (Class<T>) Element.class;
        }
    }

    public abstract void draw(T component, int mouseX, int mouseY, float partialTicks);
    public abstract void mouseClicked(T component, int mouseX, int mouseY, int mouseButton);
    public abstract void mouseReleased(T component, int mouseX, int mouseY, int state);

    public final Class<T> getType() {
        return this.type;
    }

    public final AbstractTheme getTheme() {
        return this.theme;
    }
}
