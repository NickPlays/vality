package me.oskynick.vality.ui.click.theme.cracka;

import me.oskynick.vality.ui.click.components.Frame;
import me.oskynick.vality.ui.click.components.element.Button;
import me.oskynick.vality.ui.click.theme.AbstractTheme;
import me.oskynick.vality.ui.click.theme.ElementRenderer;
import me.oskynick.vality.utils.RenderUtils;

public class CrackaButton extends ElementRenderer<Button>{

	public CrackaButton(AbstractTheme theme){
		super(theme);
	}

	@Override
	public void draw(Button component, int mouseX, int mouseY, float partialTicks){
		if(!component.isInside(mouseX, mouseY)){
			RenderUtils.rectangleBorderedGradient(component.getX(), component.getY(), component.getX() + component.getWidth(), component.getY() + component.getHeight(), 0x10696969, component.enabled ? 0xFF26C281: 0xFFECECEC, component.enabled ? 0xFF26C281 : 0xFFD2D7D3);
		}else{
			RenderUtils.rectangleBorderedGradient(component.getX(), component.getY(), component.getX() + component.getWidth(), component.getY() + component.getHeight(), 0x10696969, component.enabled ? 0xFF03A678 : 0xFFDADFE1, component.enabled ? 0xFF03A678 : 0xFFDADFE1);
		}
		component.font.drawString(component.getTitle(), component.getX() + component.getWidth() / 2 - component.font.getStringWidth(component.getTitle()) / 2, component.getY() + component.getHeight() / 2 - component.font.getHeight() / 2 - 1, 0xFF696969);
	}

	@Override
	public void mouseClicked(Button component, int mouseX, int mouseY, int mouseButton){
		
	}

	@Override
	public void mouseReleased(Button component, int mouseX, int mouseY, int state){
		
	}

}
