package me.oskynick.vality.ui.click.theme.cracka;

import java.awt.Font;

import me.oskynick.vality.font.CFontRenderer;
import me.oskynick.vality.ui.click.components.Frame;
import me.oskynick.vality.ui.click.components.element.Element;
import me.oskynick.vality.ui.click.theme.AbstractTheme;
import me.oskynick.vality.ui.click.theme.ElementRenderer;
import me.oskynick.vality.utils.RenderUtils;
import net.minecraft.client.Minecraft;
import net.minecraft.client.audio.PositionedSoundRecord;
import net.minecraft.init.SoundEvents;

public class CrackaFrame extends ElementRenderer<Frame>{

	public CrackaFrame(AbstractTheme theme){
		super(theme);
	}

	@Override
	public void draw(Frame component, int mouseX, int mouseY, float partialTicks) {
		component.width = 120;
		component.height = 15;
		int elementY = component.getY() + component.getHeight() + 2;
		int totalHeight = component.getHeight();
		//RenderUtils.rectangle(x - 1, y - 1, x + width + 1, y + height + 1, 0x9095A5A6);
        //RenderUtils.rectangleBorderedGradient(x, elementY, x + width, y + height + 75, 0x10696969, 0xFFECECEC, 0xFFD2D7D3);
		if(component.open){
			for(Element element : component.getElements()){
				totalHeight += element.getHeight() + 1;
			}
		}
        RenderUtils.rectangleBorderedGradient(component.getX(), component.getY(), component.getX() + component.getWidth(), component.getY() + totalHeight, 0x10696969, 0xFFECECEC, 0xFFD2D7D3);
        if(!component.open){
        	RenderUtils.rectangleBorderedGradient(component.getX() + 106, component.getY() + 2, component.getX() + component.getWidth() - 2, component.getY() + component.getHeight() - 2, 0xFF696969, component.isInsideOpenable(mouseX, mouseY) ? 0xFFDADFE1 : 0xFFECECEC, component.isInsideOpenable(mouseX, mouseY) ? 0xFFDADFE1 : 0xFFD2D7D3);
        }else{
        	RenderUtils.rectangleBordered(component.getX() + 106, component.getY() + 2, component.getX() + component.getWidth() - 2, component.getY() + component.getHeight() - 2, 0xFF696969, component.isInsideOpenable(mouseX, mouseY) ? 0xFF03A678 : 0xFF26C281);
        }
        component.font.drawString(component.getTitle().toUpperCase(), component.getX() + 3, component.getY() + 4, 0xFF696969);
		if (component.open){
			for (Element element : component.getElements()){
				element.setX(component.getX());
				element.setY(elementY);
				element.setHeight(25);
				element.setWidth(120);
				elementY += element.getHeight() + 1;
				element.drawElement(mouseX, mouseY, partialTicks);
			}
		}
	}

	@Override
	public void mouseClicked(Frame component, int mouseX, int mouseY, int mouseButton) {
		if (mouseX >= component.getX() + 106 && mouseX <= component.getX() + component.getWidth() - 2 && mouseY >= component.getY() + 2 && mouseY <= component.getY() + component.getHeight() - 2) {
        	if(mouseButton == 0){
        		component.open = !component.open;
        		Minecraft.getMinecraft().getSoundHandler().playSound(PositionedSoundRecord.getMasterRecord(SoundEvents.UI_BUTTON_CLICK, 1.0F));
        	}
		}
	}

	@Override
	public void mouseReleased(Frame component, int mouseX, int mouseY, int state) {
		
	}

}
