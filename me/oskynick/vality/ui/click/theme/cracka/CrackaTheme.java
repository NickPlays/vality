package me.oskynick.vality.ui.click.theme.cracka;

import me.oskynick.vality.ui.click.theme.AbstractTheme;

public class CrackaTheme extends AbstractTheme{

	public CrackaTheme(){
		super("Cracka");
	}

	@Override
	public void load(){
		this.addRenderer(new CrackaFrame(this));
		this.addRenderer(new CrackaButton(this));
	}

}
