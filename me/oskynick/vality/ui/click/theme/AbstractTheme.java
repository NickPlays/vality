package me.oskynick.vality.ui.click.theme;

import java.util.ArrayList;
import java.util.List;

import me.oskynick.vality.ui.click.components.element.Element;

/**
 * Created by Nick on 2/11/2017.
 */
public abstract class AbstractTheme {

    private final List<ElementRenderer<? extends Element>> renderers = new ArrayList<ElementRenderer<? extends Element>>();

    private final String name;

    public AbstractTheme(String name) {
        this.name = name;
        this.load();
    }

    public abstract void load();

    public final <T extends Element> void draw(T component, int mouseX, int mouseY, float partialTicks) {
        ElementRenderer<T> renderer = getRenderer(component);
        if (renderer != null) {
            renderer.draw(component, mouseX, mouseY, partialTicks);
        }
    }
    
    public final <T extends Element> void mouseClicked(T component, int mouseX, int mouseY, int mouseButton) {
        ElementRenderer<T> renderer = getRenderer(component);
        if (renderer != null) {
            renderer.mouseClicked(component, mouseX, mouseY, mouseButton);
        }
    }

    @SuppressWarnings("unchecked")
    public final <T extends Element> ElementRenderer<T> getRenderer(T component) {
        return (ElementRenderer<T>) getRenderer(component.getClass());
    }

    @SuppressWarnings("unchecked")
    public final <T extends Element> ElementRenderer<T> getRenderer(Class<T> component) {
        for (ElementRenderer<?> renderer : renderers) {
            if (renderer.getType().equals(component)) {
                ElementRenderer<T> trueRenderer = (ElementRenderer<T>) renderer;
                return trueRenderer;
            }
        }
        return null;
    }

    protected <T extends Element> void addRenderer(ElementRenderer<T> renderer) {
        if (getRenderer(renderer.getType()) == null) {
            renderers.add(renderer);
        }
    }

    public final String getName() {
        return this.name;
    }

}
