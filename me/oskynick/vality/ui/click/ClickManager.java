package me.oskynick.vality.ui.click;

import java.util.ArrayList;

import me.oskynick.vality.Vality;
import me.oskynick.vality.module.Category;
import me.oskynick.vality.module.Module;
import me.oskynick.vality.ui.click.components.Frame;
import me.oskynick.vality.ui.click.components.element.Button;
import me.oskynick.vality.ui.click.theme.AbstractTheme;
import me.oskynick.vality.ui.click.theme.cracka.CrackaTheme;
import net.minecraft.client.gui.GuiScreen;

/**
 * Created by Nick on 2/13/2017.
 */
public class ClickManager extends GuiScreen{

    public ArrayList<Frame> frames = new ArrayList<Frame>();
    public ArrayList<AbstractTheme> themes = new ArrayList<AbstractTheme>();
    public AbstractTheme theme;

    public ClickManager(){
        try {
        	theme = new CrackaTheme();
            setupFrames();
        }catch(Exception e){
            e.printStackTrace();
        }
    }
    
    public void addThemes(){
    	themes.add(new CrackaTheme());
    }

    public void setupFrames(){
    	int y = 10;
        Frame testFrame = new Frame("Test Frame", 10, 10);
        testFrame.addElement(new Button("test button"));
        Button button = new Button("Sysout Button"){
        	@Override
        	public void onToggle(){
        		System.out.println("works");
        		System.out.println(this.getTitle());
        	}
        };
        testFrame.addElement(button);
        frames.add(testFrame);
    }

    @Override
    public void drawScreen(int mouseX, int mouseY, float partialTicks){
        frames.forEach(frame -> frame.drawElement(mouseX, mouseY, partialTicks));
    }

    @Override
    public void mouseClicked(int mouseX, int mouseY, int mouseButton){
        for(Frame frame : getFrames()) frame.mouseClicked(mouseX, mouseY, mouseButton);
    }

    @Override
    public void mouseReleased(int mouseX, int mouseY, int state){
        for(Frame frame : getFrames()) frame.mouseReleased(mouseX, mouseY, state);
    }

    public ArrayList<Frame> getFrames() {
        return frames;
    }
    
    public ArrayList<AbstractTheme> getThemes(){
    	return themes;
    }
    
    public AbstractTheme getTheme(){
    	return theme;
    }
    
    public void setTheme(AbstractTheme theme){
    	this.theme = theme;
    }
}
